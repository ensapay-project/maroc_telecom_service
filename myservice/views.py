import json
import datetime

import requests
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from myservice.models import Client, Facture, FactureRequestBody
from datetime import timedelta, date
from .services import checkClient

from django.conf.urls import url
# from rest_framework_swagger.views import get_swagger_view

from django.contrib.auth.models import User, Group


# from rest_framework import viewsets
# from rest_framework import permissions
# from .serializers import AddSoldeSerializer, GetSoldeSerializer, AddFactureSerializer, GetFactureSerializer, SetStatusFactureSerializer


def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


def index(request):
    response = json.dumps([{}])
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def hello(request):
    return HttpResponse('hello', content_type='text/json')


@csrf_exempt
def getSolde(request):
    if request.method == 'POST':
        payload = json.loads(request.body)
        nmb = payload['number']
        print(nmb)
        try:
            client = Client.objects.get(number=nmb)
            response = json.dumps(
                [{'Nom': client.username, 'Numero': client.number, 'Total solde': client.balance,
                  'Date de debut': client.dateStart,
                  'Date de fin': client.dateEnd}], sort_keys=True, indent=1, default=default)
        except:
            response = json.dumps([{'Error': 'Client introuvable'}])
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def addSolde(request):
    global response
    if request.method == 'POST':
        payload = json.loads(request.body)
        nmb = payload['number']
        balance = payload['balance']
        startDate = date.today()
        days = (balance / 10 * 7).__int__()
        endDate = date.today() + timedelta(days=days)
        try:
            if checkClient(nmb):
                print("client found")
                print(nmb)
                client = Client.objects.get(number=nmb)
                client.balance = client.balance + balance
                client.dateEnd = client.dateEnd + timedelta(days=days)
                client.save()
            else:
                username = payload['username']
                client = Client(username=username, number=nmb, balance=balance, dateStart=startDate, dateEnd=endDate)
                client.save()

            response = json.dumps({'number': client.number,
                                   'username': client.username,
                                   'balance': client.balance,
                                   'dateStart': client.dateStart,
                                   'dateEnd': client.dateEnd}, sort_keys=True, indent=1, default=default)
        except:
            response = json.dumps({'message': 'Error'})
    return HttpResponse(response, content_type='application/json')


def get_solde(request, number):
    if request.method == 'GET':
        print(number)
        try:
            client = Client.objects.get(number=float(number))
            response = json.dumps(
                {'username': client.username, 'number': client.number, 'balance': client.balance,
                 'dateStart': client.dateStart,
                 'dateEnd': client.dateEnd}, sort_keys=True, indent=1, default=default)
        except:
            message = "Introuvable!"
            response = json.dumps(
                {'message': message}, sort_keys=True, indent=1, default=default)
    return HttpResponse(response, content_type="application/json")


# class ClientViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows clients to be viewed or edited.
#     """
#     queryset = Client.objects.all()
#     serializer_class = ClientSerializer
#     permission_classes = [permissions.IsAuthenticated]


@csrf_exempt
def getFacture(request):
    if request.method == 'GET':
        payload = json.loads(request.body)
        userId = payload['userId']
        facture = Facture.objects.get(userId=userId)
        print(facture)
        try:
            print(userId)
            response = json.dumps(
                [{'userId': facture.userId,
                  'montant': facture.montant,
                  'fraisPenalite': facture.fraisPenalite,
                  'status': facture.status,
                  'dateLimit': facture.dateLimit,
                  'description': facture.description
                  }],
                sort_keys=True, indent=1, default=default)
            rep = requests.post('http://localhost:8000/factures/external/1', data=response)
            print(rep.content)

        except:
            response = json.dumps([{'Error': 'Facture introuvable'}])
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def addFacture(request):
    global response
    if request.method == 'POST':
        payload = json.loads(request.body)
        userid = payload['userId']
        montant = payload['montant']
        fraisPenalite = payload['fraisPenalite']
        # status = payload['status']
        # dateLimit = payload['dateLimit']
        dateLimit = date.today()
        description = payload['description']

        try:
            print(payload)
            facture = FactureRequestBody(userId=userid,
                                         montant=montant,
                                         fraisPenalite=fraisPenalite,
                                         dateLimit=dateLimit,
                                         status=False,
                                         description=description)
            facture.save()
            response = json.dumps({'userId': facture.userId,
                                   'montant': facture.montant,
                                   'fraisPenalite': facture.fraisPenalite,
                                   'status': facture.status,
                                   'dateLimit': facture.dateLimit,
                                   'description': facture.description}, sort_keys=True, indent=1, default=default)

        except:
            response = json.dumps({'message': 'Error'})
    return HttpResponse(response, content_type='application/json')


@csrf_exempt
def setStatusFacture(request):
    if request.method == 'PUT':
        payload = json.loads(request.body)
        userId = payload['userId']
        newStatus = payload['status']
        print(newStatus)
        facture = Facture.objects.get(userId=userId)
        if newStatus == 'PAID':
            facture.status = newStatus
            Facture.save(facture)
        try:
            response = json.dumps(
                [{'userId': facture.userId,
                  'montant': facture.montant,
                  'fraisPenalite': facture.fraisPenalite,
                  'status': facture.status,
                  'dateLimit': facture.dateLimit,
                  'description': facture.description
                  }],
                sort_keys=True, indent=1, default=default)
        except:
            response = json.dumps([{'Error': 'Facture introuvable'}])
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def sendFacture(request):
    global response
    if request.method == 'POST':
        payload = json.loads(request.body)
        userId = payload['userId']
        facture = FactureRequestBody.objects.get(userId=userId)
        print(facture)
        try:
            print(userId)
            response = json.dumps(
                {'userId': facture.userId,
                 'montant': facture.montant,
                 'fraisPenalite': facture.fraisPenalite,
                 'payee': facture.status,
                 'dateLimit': facture.dateLimit,
                 'description': facture.description
                 },
                sort_keys=True, indent=1, default=default)
            headers = {'Content-type': 'application/json'}

        except:
            response = json.dumps([{'Error': 'Facture introuvable'}])
    return HttpResponse(requests.post('http://localhost:8000/factures/external/1', data=response, headers={'Content-type': 'application/json'}), content_type='application/json')

# class AddFactureViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows clients to be viewed or edited.
#     """
#     queryset = Facture.objects.all()
#     serializer_class = AddFactureSerializer
#     permission_classes = [permissions.IsAuthenticated]
#
#
# class GetFactureViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows clients to be viewed or edited.
#     """
#     queryset = Facture.objects.all()
#     serializer_class = GetFactureSerializer
#     permission_classes = [permissions.IsAuthenticated]
#
#
# class SetStatusFactureViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows clients to be viewed or edited.
#     """
#     queryset = Facture.objects.all()
#     serializer_class = SetStatusFactureSerializer
#     permission_classes = [permissions.IsAuthenticated]
