import uuid

from django.db import models
from enum import Enum


# Not for use at this moment
class Client(models.Model):
    username = models.CharField(max_length=100)
    number = models.IntegerField()
    balance = models.IntegerField()
    dateStart = models.DateField()
    dateEnd = models.DateField()


class FactureStatus(Enum):
    PAYED = "PAYED"
    UNPAID = "UNPAID"

    @classmethod
    def choices(cls):
        print(tuple((i.name, i.value) for i in cls))
        return tuple((i.name, i.value) for i in cls)


class Facture(models.Model):
    factureId = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    userId = models.IntegerField()
    montant = models.IntegerField()
    fraisPenalite = models.IntegerField()
    status = models.CharField(max_length=200, choices=FactureStatus.choices())
    dateLimit = models.DateField()
    description = models.CharField(max_length=250)


class FactureRequestBody(models.Model):
    userId = models.IntegerField()
    montant = models.IntegerField()
    fraisPenalite = models.IntegerField()
    status = models.BooleanField(default=False)
    dateLimit = models.DateField()
    description = models.CharField(max_length=250)
