from django.contrib.auth.models import User, Group
# from rest_framework import serializers
from myservice.models import Client, Facture


# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ['url', 'username', 'email', 'groups']
#
#
# class GroupSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Group
#         fields = ['url', 'name']


# class ClientSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Client
#         fields = ['url', 'username', 'number', 'balance', 'dateStart', 'dateEnd']

#
# class AddSoldeSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Client
#         fields = ['url', 'username', 'number', 'balance', 'dateStart', 'dateEnd']
#
#
# class GetSoldeSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Client
#         fields = ['url', 'username', 'number', 'balance', 'dateStart', 'dateEnd']
#
#
# class AddFactureSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Facture
#         fields = ['userId', 'montant', 'fraisPenalite', 'status', 'dateLimit', 'description']
#
#
# class GetFactureSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Facture
#         fields = ['factureId', 'userId', 'montant', 'fraisPenalite', 'status', 'dateLimit', 'description']
#
#
# class SetStatusFactureSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Facture
#         fields = ['status']
