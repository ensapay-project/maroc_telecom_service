# README #


### What is this repository for? ###



* MAROC TELECOM SERVER is for getting and adding balance to client account (JSON Responses).

### How i can run my server? ###

* Clone and open your project : cd Maroc_Telecom_service
* Install [Python](https://www.python.org/downloads/) (Python3 recommended) 
* Install the requirements : apt-get install [requirements.txt](https://bitbucket.org/ensapay-project/maroc_telecom_service/src/master/requirements.txt)
* run project : python3 manage.py runserver
* open : [localhost:8000/](localhost:8000/) (8000 is the default port you can change by running your project with : python3 manage.py runserver <yourPort>)

### APIS Docummentations : Swagger###

* After you run your project
* open : [localhost:8000/docs/](localhost:8000/docs/)
* PS : Only GET [/getsolde/](localhost:8000/getsolde/) and POST [/addsolde/](localhost:8000/addsolde/) is working right now!

![Scheme](swagger.png)
