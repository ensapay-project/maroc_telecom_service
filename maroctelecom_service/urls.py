"""maroctelecom_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.db import router
from django.urls import path, include
from myservice import views
# from rest_framework.schemas import get_schema_view
# from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer
from django.views.generic import TemplateView
# from rest_framework_swagger.views import get_swagger_view

# schema_view = get_swagger_view(title='Maroc Telecom Service')
from django.urls import include, path
# from rest_framework import routers
# from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

# router = routers.DefaultRouter()
# router.register(r"users", views.UserViewSet)
# router.register(r"groups", views.GroupViewSet)
# router.register(r"clients", views.ClientViewSet)
# router.register(r"Add Facture", views.AddFactureViewSet)
# router.register(r"Get Facture", views.GetFactureViewSet)
# router.register(r"Set Facture", views.SetStatusFactureViewSet)

urlpatterns = [
    # path("", include(router.urls)),
    # path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path('admin/', admin.site.urls),
    path('add-solde', views.addSolde),
    path('get-solde', views.getSolde),
    path('add-facture', views.addFacture),
    path('get-facture', views.getFacture),
    path('send-facture', views.sendFacture),
    path('set-facture', views.setStatusFacture),
    path('<str:number>', views.get_solde),
    path('hello', views.hello),
# OpenAPI 3 documentation with Swagger UI
#     path("schema/", SpectacularAPIView.as_view(), name="schema"),
#     path(
#         "docs/",
#         SpectacularSwaggerView.as_view(
#             template_name="swagger-ui.html", url_name="schema"
#         ),
#         name="swagger-ui",
#     ),

    # path('docs/', TemplateView.as_view(
    #     template_name='documentation.html',
    #     extra_context={'schema_url': 'openapi-schema'}
    # ), name='swagger-ui'),
]
